//
//  FillingFormView.swift
//  Gap Estimator
//
//  Created by Robert Szost on 20/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class FillingFormView: UIView {
    
    let fillingFormViewModel: FillingFormViewModel
    
    lazy var applicationTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Gap Estimator"
        label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        label.textAlignment = .left
        label.font = UIFont (name: "HelveticaNeue-Medium", size: 24)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Why don't you calculate some distance?"
        label.numberOfLines = 0
        label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        label.textAlignment = .left
        label.font = UIFont (name: "HelveticaNeue-Thin", size: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var titleAndSubtitleStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [applicationTitleLabel, subtitleLabel])
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = 5
        stackView.distribution = .equalCentering
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    lazy var pointingBoyImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = #imageLiteral(resourceName: "imageForMainScreen")
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var mainBoxStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [pointOneStackView, pointTwoStackView])
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    //should move somewhere else
    lazy var pointOneLabel: UILabel = {
        let label = UILabel()
        label.text = "Coordinates for first point: "
        label.font = UIFont (name: "HelveticaNeue-Medium", size: 16)
        label.textColor = #colorLiteral(red: 0.4966713587, green: 0.7892006876, blue: 1, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var pointOneLatitudeTextField: UITextField = {
        let textField = UITextField()
        textField.tag = 1001
        textField.setLayoutForTextField(placeholder: "Latitude")
        textField.setBottomBorder()
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    lazy var pointOneLongitudeTextField: UITextField = {
        let textField = UITextField()
        textField.tag = 1002
        textField.setLayoutForTextField(placeholder: "Longitude")
        textField.setBottomBorder()
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    lazy var pointOneStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [pointOneLabel, pointOneLatitudeTextField, pointOneLongitudeTextField])
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = 5
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    lazy var pointTwoLabel: UILabel = {
        let label = UILabel()
        label.text = "Coordinates for second point: "
        label.font = UIFont (name: "HelveticaNeue-Medium", size: 16)
        label.textColor = #colorLiteral(red: 0.497220397, green: 0.7903195024, blue: 0.9985197186, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var pointTwoLatitudeTextField: UITextField = {
        let textField = UITextField()
        textField.tag = 2001
        textField.setLayoutForTextField(placeholder: "Latutude")
        textField.setBottomBorder()
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    lazy var pointTwoLongitudeTextField: UITextField = {
        let textField = UITextField()
        textField.tag = 2002
        textField.setLayoutForTextField(placeholder: "Longitude")
        textField.setBottomBorder()
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    lazy var pointTwoStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [pointTwoLabel, pointTwoLatitudeTextField, pointTwoLongitudeTextField])
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = 5
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    lazy var calculateDistanceButton: UIButton = {
        let button = UIButton()
        button.setTitle("Calculate Distance", for: .normal)
        button.titleLabel?.font = UIFont (name: "HelveticaNeue-Medium", size: 14)
        button.titleLabel?.textColor = .white
        button.backgroundColor = #colorLiteral(red: 0.1973879039, green: 0.6638576388, blue: 1, alpha: 1)
        button.contentMode = .center
        button.layer.cornerRadius = 10
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var backgroundForStackView: UIView = {
       let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9645769, green: 0.9646194577, blue: 0.9727542996, alpha: 1)
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        self.backgroundColor = #colorLiteral(red: 0.2804217935, green: 0.3646888733, blue: 0.8630130887, alpha: 1)
        setupDelegates()
        createHierarchy()
        setupConstraints()
        bindView()
    }
    
    private func setupDelegates() {
        pointOneLatitudeTextField.delegate = fillingFormViewModel
        pointOneLongitudeTextField.delegate = fillingFormViewModel
        pointTwoLatitudeTextField.delegate = fillingFormViewModel
        pointTwoLongitudeTextField.delegate = fillingFormViewModel
    }
    
    init(frame: CGRect = .zero, fillingFormViewModel: FillingFormViewModel) {
        self.fillingFormViewModel = fillingFormViewModel
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func bindView() {
        calculateDistanceButton.addTarget(fillingFormViewModel, action: #selector(fillingFormViewModel.calculateDistanceButtonTapped), for: .touchUpInside)
    }
    
    private func createHierarchy() {
        addSubview(titleAndSubtitleStackView)
        addSubview(pointingBoyImageView)
        addSubview(backgroundForStackView)
        backgroundForStackView.addSubview(mainBoxStackView)
        backgroundForStackView.addSubview(calculateDistanceButton)
    }
    
    private func setupConstraints() {
        setupConstraintsTitleAndSubtitleStackView()
        setupConstraintsPointingBoyImageView()
        setupConstraintsMainBoxContainerView()
        setupConstraintsBackgroundForStackView()
        setupConstraintsCalculateDistanceButton()
    }
    
    private func setupConstraintsTitleAndSubtitleStackView() {
        NSLayoutConstraint.activate([
            titleAndSubtitleStackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            titleAndSubtitleStackView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.5),
            titleAndSubtitleStackView.centerYAnchor.constraint(equalTo: pointingBoyImageView.centerYAnchor)
        ])
    }
    
    private func setupConstraintsPointingBoyImageView() {
        NSLayoutConstraint.activate([
            pointingBoyImageView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor, constant: UIScreen.main.bounds.height * 0.1),
            pointingBoyImageView.leadingAnchor.constraint(equalTo: titleAndSubtitleStackView.trailingAnchor),
            pointingBoyImageView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            pointingBoyImageView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height * 0.20)
        ])
    }
    
    private func setupConstraintsMainBoxContainerView() {
        NSLayoutConstraint.activate([
            mainBoxStackView.topAnchor.constraint(equalTo: backgroundForStackView.layoutMarginsGuide.topAnchor),
            mainBoxStackView.leadingAnchor.constraint(equalTo: backgroundForStackView.layoutMarginsGuide.leadingAnchor),
            mainBoxStackView.trailingAnchor.constraint(equalTo: backgroundForStackView.layoutMarginsGuide.trailingAnchor),
            mainBoxStackView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height * 0.3)
        ])
    }

    private func setupConstraintsBackgroundForStackView() {
        NSLayoutConstraint.activate([
            backgroundForStackView.topAnchor.constraint(equalTo: pointingBoyImageView.bottomAnchor, constant: UIScreen.main.bounds.height * 0.1),
            backgroundForStackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            backgroundForStackView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            backgroundForStackView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: UIScreen.main.bounds.width * -0.1),
        ])
    }
    
    private func setupConstraintsCalculateDistanceButton() {
        NSLayoutConstraint.activate([
            calculateDistanceButton.topAnchor.constraint(equalTo: mainBoxStackView.bottomAnchor, constant: UIScreen.main.bounds.height * 0.1),
            calculateDistanceButton.trailingAnchor.constraint(equalTo: mainBoxStackView.layoutMarginsGuide.trailingAnchor),
            calculateDistanceButton.widthAnchor.constraint(equalToConstant: self.bounds.width * 0.4)
        ])
    }
}
