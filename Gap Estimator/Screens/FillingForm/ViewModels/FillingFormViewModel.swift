//
//  FillingFormViewModel.swift
//  Gap Estimator
//
//  Created by Robert Szost on 20/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class FillingFormViewModel: NSObject {
    var startingLocation = Location(longitude: 0.0, latitude: 0.0)
    var endingLocation = Location(longitude: 0.0, latitude: 0.0)
    var presentResultsOfCalculationViewController: (() -> Void)?
    
    @objc func calculateDistanceButtonTapped(){
        presentResultsOfCalculationViewController!()
    }
}

extension FillingFormViewModel: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
            case 1001:
                let value = (textField.text! as NSString).doubleValue
                startingLocation.latitude = value
                print(startingLocation.latitude)
            case 1002:
                print(textField.text!)
                let value = (textField.text! as NSString).doubleValue
                startingLocation.longitude = value
        case 2001:
                print(textField.text!)
                let value = (textField.text! as NSString).doubleValue
                endingLocation.latitude = value
        case 2002:
                print(textField.text!)
                let value = (textField.text! as NSString).doubleValue
                endingLocation.longitude = value
            default:
                print("Something really wrong happend!")
            }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let changeCommaToDot = textField.text!.replacingOccurrences(of: ",", with: ".", options: .literal, range: nil)
        textField.text = changeCommaToDot
        return true
    }
}
