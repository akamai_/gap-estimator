//
//  FillingFormViewController.swift
//  Gap Estimator
//
//  Created by Robert Szost on 20/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class FillingFormViewController: UIViewController {
    var fillingFormViewModel: FillingFormViewModel!

    init(fillingFormViewModel: FillingFormViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.fillingFormViewModel = fillingFormViewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override  func loadView() {
            view = FillingFormView(fillingFormViewModel: fillingFormViewModel)
    }
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()
        bindNavigation()
        hideKeyboardWhenTappedAround()
    }
    
    func bindNavigation() {
        fillingFormViewModel?.presentResultsOfCalculationViewController = { [weak self] in
            guard let self = self else { return }
            if self.validateRangeOfCoordinates() {
                var nameOfFirstLocation = self.getNamesForPoint(point: self.fillingFormViewModel.startingLocation)
                var nameOfSecondLocation = self.getNamesForPoint(point: self.fillingFormViewModel.endingLocation)
                var resultsOfCalculationViewContoller = ResultSOfCalculationViewContoller(resultsOfCalulationViewModel: ResultsOfCalculationViewModel())
                resultsOfCalculationViewContoller.modalPresentationStyle = .automatic
                self.navigationController?.pushViewController(resultsOfCalculationViewContoller, animated: true)
            }
        }
    }
    
    func getNamesForPoint(point: Location) {
        NetworkService.shared.getLocationName(longitude: point.longitude, latitude: point.latitude) {(result) in
            switch result {
            case .failure(let error):
                print(error.errorDescription)
            case .success(let result):
                print(result)
            }
        }
    }
    
    func validateRangeOfCoordinates() -> Bool {
        //Checking range for latitude
        if self.fillingFormViewModel.startingLocation.latitude < -90 || self.fillingFormViewModel.startingLocation.latitude > 90{
                   let alert = UIAlertController(
                       title: "Error",
                       message: "Please use latitude in correct range\n\nPossible range: -90° and 90°",
                       preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                   present(alert, animated: true, completion: nil)
                   alert.addAction(cancelAction)
            return false
        }
        
        //Checking range for longitude
        if self.fillingFormViewModel.startingLocation.longitude < -180 || self.fillingFormViewModel.startingLocation.longitude > 180{
                   let alert = UIAlertController(
                       title: "Error",
                       message: "Please use longitude in correct range\n\nPossible range: -180° and 180°",
                       preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                   present(alert, animated: true, completion: nil)
                   alert.addAction(cancelAction)
            return false

        }
        return true
    }
}

