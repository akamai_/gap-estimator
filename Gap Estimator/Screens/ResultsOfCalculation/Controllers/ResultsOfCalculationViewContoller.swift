//
//  ResultsOfCalculationViewContoller.swift
//  Gap Estimator
//
//  Created by Robert Szost on 21/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class ResultSOfCalculationViewContoller: UIViewController {
        var resultsOfCalulationViewModel : ResultsOfCalculationViewModel!
        
        init(resultsOfCalulationViewModel: ResultsOfCalculationViewModel) {
            super.init(nibName: nil, bundle: nil)
            self.resultsOfCalulationViewModel = resultsOfCalulationViewModel
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
            
        public override  func loadView() {
            view = ResultsOfCalulationView(resultsOfCalculationViewModel: resultsOfCalulationViewModel)
        }
            
        override func viewDidLoad() {
            super.viewDidLoad()
            bindNavigation()
            hideKeyboardWhenTappedAround()
        }
            
        func bindNavigation() {
            resultsOfCalulationViewModel?.presentFillingFormViewController = { [weak self] in
                guard let self = self else { return }
                self.navigationController?.popToRootViewController(animated: true)
                }
        }
    }
