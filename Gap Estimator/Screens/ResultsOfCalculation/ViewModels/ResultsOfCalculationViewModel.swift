//
//  ResultsOfCalculationViewModel.swift
//  Gap Estimator
//
//  Created by Robert Szost on 21/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class ResultsOfCalculationViewModel: NSObject {
    var presentFillingFormViewController: (() -> Void)?
    
    @objc func dismissButtonTapped(){
        presentFillingFormViewController!()
    }
    
}
