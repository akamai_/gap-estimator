//
//  ResultsOfCalulationView.swift
//  Gap Estimator
//
//  Created by Robert Szost on 21/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

class ResultsOfCalulationView: UIView {
    
        var resultsOfCalculationViewModel: ResultsOfCalculationViewModel
        
        lazy var nameOfLocationOne: UILabel = {
        let label = UILabel()
        label.text = "Name of location 1"
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7957606403)
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        }()
        
        lazy var nameOfLocationTwo: UILabel = {
            let label = UILabel()
            label.text = "Name of location 2"
            label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7957606403)
            label.textAlignment = .left
            label.font = .systemFont(ofSize: 16)
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()

        lazy var testButton: UIButton = {
            let button = UIButton()
            button.setTitle("Dismiss", for: .normal)
            button.titleLabel?.font = UIFont (name: "HelveticaNeue-Medium", size: 14)
            button.titleLabel?.textColor = .white
            button.backgroundColor = #colorLiteral(red: 0.1973879039, green: 0.6638576388, blue: 1, alpha: 1)
            button.contentMode = .center
            button.layer.cornerRadius = 10
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }()
        override func didMoveToWindow() {
            super.didMoveToWindow()
            self.backgroundColor = #colorLiteral(red: 0.2804217935, green: 0.3646888733, blue: 0.8630130887, alpha: 1)
            createHierarchy()
            setupConstraints()
            bindView()
        }
    
        init(frame: CGRect = .zero, resultsOfCalculationViewModel: ResultsOfCalculationViewModel) {
            self.resultsOfCalculationViewModel = resultsOfCalculationViewModel
            super.init(frame: frame)
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    private func bindView() {
        testButton.addTarget(resultsOfCalculationViewModel, action: #selector(resultsOfCalculationViewModel.dismissButtonTapped), for: .touchUpInside)

    }
    
        private func createHierarchy() {
            self.addSubview(nameOfLocationOne)
            self.addSubview(nameOfLocationTwo)
            self.addSubview(testButton)

        }
        
        private func setupConstraints() {
            setupConstraintsTasksNotFoundImage()
            setupConstraintsTitleLabel()
            setupConstraintsAddNewTaskButton()
        }
        
        private func setupConstraintsTasksNotFoundImage() {
            NSLayoutConstraint.activate([
                nameOfLocationOne.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
                nameOfLocationOne.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor, constant: frame.height * -0.05),
            ])
        }

        
        private func setupConstraintsTitleLabel() {
            NSLayoutConstraint.activate([
                nameOfLocationTwo.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
                nameOfLocationTwo.topAnchor.constraint(equalTo: nameOfLocationOne.bottomAnchor, constant: frame.height * 0.03)
            ])
        }
        
        private func setupConstraintsAddNewTaskButton() {
            NSLayoutConstraint.activate([
                testButton.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
                testButton.widthAnchor.constraint(equalToConstant: frame.width * 0.4),
                testButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: frame.height * -0.05)
            ])
        }
    }
