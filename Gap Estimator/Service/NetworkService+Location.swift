//
//  NetworkService+Location.swift
//  Gap Estimator
//
//  Created by Robert Szost on 21/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import Foundation

extension NetworkService {
    
    func getLocationName(longitude: Double, latitude: Double, completion: @escaping (Result<String, NetworkError>) -> ()) {
            let session = URLSession(configuration: configuration)
            let pathComponent = "/reverse?format=jsonv2&lat=\(latitude)&lon=\(longitude)"
            let fullUrl = URL(string: baseUrl + pathComponent)!

            let task = session.dataTask(with: fullUrl) { (data, response, error) in
                DispatchQueue.main.async {
                    guard let httpResponse = response as? HTTPURLResponse,
                        httpResponse.statusCode == 200 else {
                            completion(.failure(.BadResponse))
                            return
                    }
                    guard let data = data else {
                        completion(.failure(.BadData))
                        return
                    }
                    
                    if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        if let name = json["name"] as? String {
                        completion(.success(name))
                        }
                    } else {
                        completion(.failure(.JSONError))
                    } 
                }
            }
            task.resume()
        }
}
