//
//  NetworkService.swift
//  Gap Estimator
//
//  Created by Robert Szost on 21/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case NoNetwork
    case BadResponse
    case JSONError
    case BadData
    case NoData
    
    public var errorDescription: String {
        switch self {
            case .NoNetwork:
                return "There is no internet connection"
            case .BadResponse:
                return "Bad response"
        case .BadData:
                return "Bad data"
            case .JSONError:
                return "Couldn't parse the JSON"
            case .NoData:
                return "No Data"
        }
    }
}

class NetworkService {

    static let shared = NetworkService()
    
    let baseUrl = "https://nominatim.openstreetmap.org"
    
    private init() {}
    
    //MARK: Session Configuration
    let configuration: URLSessionConfiguration = {
        let conf = URLSessionConfiguration.default
        conf.waitsForConnectivity = true
        conf.allowsCellularAccess = true
        return conf
    }()
}
