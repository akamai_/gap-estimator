//
//  UIViewController+KeyboardHandler.swift
//  Gap Estimator
//
//  Created by Robert Szost on 20/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}
