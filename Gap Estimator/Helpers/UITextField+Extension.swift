//
//  UITextField+Extension.swift
//  Gap Estimator
//
//  Created by Robert Szost on 20/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import UIKit

extension UITextField {
    func setLayoutForTextField(placeholder: String) {
        self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: UIColor(cgColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1976610429))])
        self.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.3).isActive = true
        self.textColor = .black
        self.keyboardType = .decimalPad
        self.textAlignment = .left
    }

  func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = #colorLiteral(red: 0.9645769, green: 0.9646194577, blue: 0.9727542996, alpha: 1)
        self.layer.masksToBounds = false
        self.layer.shadowColor =  #colorLiteral(red: 0.4966713587, green: 0.7892006876, blue: 1, alpha: 1)
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
  }
}
