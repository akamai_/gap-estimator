//
//  Location.swift
//  Gap Estimator
//
//  Created by Robert Szost on 20/05/2020.
//  Copyright © 2020 Robert Szost. All rights reserved.
//

import Foundation

struct Location: Decodable, Hashable {
    var longitude: Double
    var latitude: Double
    var name: String?
    
    init(longitude: Double, latitude: Double) {
        self.longitude = longitude
        self.latitude = latitude
    }
}
